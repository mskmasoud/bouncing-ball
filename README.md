![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---
# Bouncing Ball
Simple game with plain HTML/CSS/JS. [Live demo here!](https://mskmasoud.gitlab.io/bouncing-ball/)

### Controls:
-  **ArrowLeft:** horizontal speed increase to left
-  **ArrowRight:** horizontal speed increase to right
-  **ArrowDown:** horizontal speed decrease
-  **ArrowUp:** horizontal speed increase
-  **Click:** pause/resume